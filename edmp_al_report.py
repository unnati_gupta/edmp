#!/usr/bin/env python
# coding: utf-8

import os
import sys
import time
import json
import random
from datetime import datetime

import paramiko
import psycopg2
import pandas as pd

from stanc_db_const import POSTGRES_SOURCE_TABLE as pg_cred, EDMP_FTP_HOST, EDMP_FTP_USERNAME, EDMP_FTP_PORT, \
  EDMP_FTP_PRIVATE_KEY_FILE_PATH, EDMP_FTP_REMOTE_FILE_PATH
# from stanc_constants import POSTGRES_SOURCE_TABLE as pg_cred
from stanc_constants import AL_PROC_VARS
from helpers import send_attachment_email

INITIAL_TIME = "2020-09-18 00:00:00.000"
# END_TIME = "2020-04-20 00:00:00.000"
END_TIME = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#START_TIME_STR = "2020-08-15 00:00:00.000"

filePath = "/scb-etl/edmp/"

sleep_time = random.randint(1, 100)
print(f"sleep time(in seconds): {sleep_time}")
time.sleep(sleep_time)

CREATE_ETL_LOGS_SQL = """
           CREATE TABLE if not exists sc_edmp_al_etl_logs(
           id serial PRIMARY KEY,
           start_time timestamp NOT NULL,
           end_time timestamp NOT NULL,
           processed_date timestamp NOT NULL DEFAULT NOW(),
           status text);
        """

SELECT_SQL = "select processed_date from sc_edmp_al_etl_logs where processed_date > NOW() - INTERVAL '1 HOURS';"

try:
    pg_conn = psycopg2.connect(f"dbname={pg_cred['dbname']} user={pg_cred['user']} password={pg_cred['password']} host={pg_cred['host']} port={pg_cred['port']}")
    pg_cur = pg_conn.cursor()
    pg_cur.execute(CREATE_ETL_LOGS_SQL)
    pg_cur.execute(SELECT_SQL)
    pg_conn.commit()
    records = pg_cur.fetchall()
    pg_conn.close()
except Exception as e:
    print(e)
    pg_conn.close()
    sys.exit(1)
if len(records) > 0:
    print("Script is already executed from parallel server... exiting from here")
    sys.exit(0)

insert_time_sql = f"""
        INSERT into sc_edmp_al_etl_logs (start_time, end_time, processed_date, status)
        (SELECT
            CASE
                WHEN MAX(end_time) is null THEN '{INITIAL_TIME}'
                ELSE MAX(end_time)
            END,
            '{END_TIME}', '{END_TIME}', 'Processing'
        FROM  sc_edmp_al_etl_logs);
"""

try:
    pg_conn = psycopg2.connect(f"dbname={pg_cred['dbname']} user={pg_cred['user']} password={pg_cred['password']} host={pg_cred['host']} port={pg_cred['port']}")
    pg_cur = pg_conn.cursor()
    pg_cur.execute(insert_time_sql)
    pg_conn.commit()

    GET_LATEST_ENTRY_IN_ETL_SQL = f"""select id as ID , start_time as START_TIME, status as STATUS from sc_edmp_al_etl_logs order by id desc limit 1"""
    pg_cur.execute(GET_LATEST_ENTRY_IN_ETL_SQL)
    MAX_ID, START_TIME, STATUS = pg_cur.fetchone()
    START_TIME_STR = START_TIME.strftime("%Y-%m-%d %H:%M:%S")
    pg_conn.close()
except Exception as e:
    print(e)
    pg_conn.close()
    sys.exit(1)

print(f"Script will fetch data from DB for time duration: {START_TIME_STR} TO {END_TIME}")

print(f"Script Execution Time: {END_TIME}")

# this file contains the EDMP BIL's columns and its corresponding business names.
total_edmp_columns = pd.read_csv(filePath + "total_edmp_al_columns_mappings.csv")

GET_PROC_IDS_SQL = f"""
SELECT DISTINCT proc_inst_id_
FROM
    act_hi_varinst
WHERE
    last_updated_time_ > '{START_TIME_STR}'
    AND
    last_updated_time_ <= '{END_TIME}'
"""

GET_SCOPE_IDS_SQL = f"""
SELECT DISTINCT scope_id_
FROM
    act_hi_varinst
WHERE
    last_updated_time_ > '{START_TIME_STR}'
    AND
    last_updated_time_ <= '{END_TIME}'
"""


def get_id_list(pg_cur):
    """Given postgres cursor, return list of ids.
    :param - pg_cur postgres cursor.
    """
    _id_list = []
    for row in pg_cur.fetchall():
        _id_list.append(row[0])
    return _id_list


def get_id_list_as_tuple(_id_list):
    """Return string tuple of list.
    :param - _id_list (list): list of IDs.
    """
    if len(_id_list) == 1:
        _id_str = f"('{_id_list[0]}')"
    elif len(_id_list) > 1:
        _id_str = f"{tuple(_id_list)}"
    else:
        _id_str = "('')"
    return _id_str


def convert_to_json(json_string):
    """Return json for given string content.
    :param - json_string(str): Valid Json String.
    """
    if json_string:
        try:
            json_string = json.loads(json_string)
        except Exception as e:
            print("Cant convert to json... returning empty json")
            json_string = dict()
    else:
        json_string = dict()
    return json_string


def getListOfValues(row, key, count):
    rowValue = None
    if key in row.index:
        rowValue = row[key]
    if len(rowValue) >= count:
        return rowValue[:count]
    else:
        rowValue.extend([None] * (count - len(rowValue)))
        return rowValue


def ftp_transfer(report_name):
    """ Transfer file to different server.
    :param report_name (str): name of the report name.
    """
    try:
        key = paramiko.RSAKey.from_private_key_file(EDMP_FTP_PRIVATE_KEY_FILE_PATH)
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(hostname=EDMP_FTP_HOST, username=EDMP_FTP_USERNAME, port=EDMP_FTP_PORT, pkey=key)
        sftp = ssh_client.open_sftp()
        sftp.put(report_name, EDMP_FTP_REMOTE_FILE_PATH + f"/{report_name.split('/')[-1]}")
        sftp.close()
        ssh_client.close()
    except Exception as e:
        print("ERROR: ERROR in connecting to remote server")
        print(e)
        sftp.close()
        ssh_client.close()


def create_edmp_al_report(empty_report=False): 
    print("In creating edmp report for AL....")
    edmp_al_final_df = pd.DataFrame(columns=list(total_edmp_columns["db_name"]))
    if not empty_report:
        result_var_df.fillna('', inplace=True)
        edmp_al_final_df = result_var_df.copy()
    edmp_al_final_df = edmp_al_final_df[total_edmp_columns["db_name"]]
    edmp_al_final_df.index.name = "Serial_Number"

    today_date = datetime.today().strftime('%Y%m%d')
    if not os.path.exists(filePath + 'edmp_reports'):
        os.makedirs(filePath + 'edmp_reports')
    report_path = os.path.abspath(filePath + "edmp_reports")
    csv_file_name = os.path.join(report_path, f"SG_KUL_ACTHIVAR_AL_{today_date}.csv")
    dat_file_name = os.path.join(report_path, f"SG_KUL_ACTHIVAR_AL_{today_date}.dat")

    edmp_al_final_df.to_csv(csv_file_name, sep=',', quotechar='"')

    edmp_al_final_df['recommendedLoanAmount'] = pd.to_numeric(edmp_al_final_df['recommendedLoanAmount'], errors='coerce')
    sum_of_loan_amount = int(edmp_al_final_df['recommendedLoanAmount'].sum())

    edmp_al_dat_header = ["H", "1.1", "1.1", "SG", today_date, today_date, "1"]
    no_of_rows = edmp_al_final_df.shape[0]
    loan_amount_index = edmp_al_final_df.columns.get_loc('recommendedLoanAmount') + 1
    no_of_checksum = 1
    edmp_al_dat_trailer = ["T", f"{no_of_rows}", f"{no_of_checksum}", f"{loan_amount_index}", f"{sum_of_loan_amount}"]

    edmp_al_final_df["DataIdentifier"] = "D"
    edmp_al_final_df.set_index('DataIdentifier', inplace=True)

    with open(f"{dat_file_name}", "w") as m:
        m.write(chr(1).join(edmp_al_dat_header) + "\n")  # writing the header to the file.
    edmp_al_final_df.to_csv(f"{dat_file_name}", header=None, mode="a", index_label="D", sep=chr(1))  # appending data to the file
    with open(f"{dat_file_name}", "a") as m:
        m.write(chr(1).join(edmp_al_dat_trailer))  # appending the trailer to the file
    print("EDMP report done for AL.")
    return csv_file_name, dat_file_name


try:
    pg_conn = psycopg2.connect(f"dbname={pg_cred['dbname']} user={pg_cred['user']} password={pg_cred['password']} host={pg_cred['host']} port={pg_cred['port']}")
    pg_cur = pg_conn.cursor()

    pg_cur.execute(GET_PROC_IDS_SQL)
    proc_id_list = get_id_list(pg_cur)

    pg_cur.execute(GET_SCOPE_IDS_SQL)
    scope_id_list = get_id_list(pg_cur)

    print("AL proc Id count :", len(proc_id_list))
    print("AL scope Id count:", len(scope_id_list))

    if None in proc_id_list:
        proc_id_list.remove(None)
    if None in scope_id_list:
        scope_id_list.remove(None)

    pg_conn.close()
except Exception as e:
    print(e)
    print("ERROR IN FETCHING ID LIST")
    error_message = f"ERROR IN FETCHING ID LIST: {e}"
    print(error_message)
    pg_conn.close()
    sys.exit(1)

# proc_id_list = ["84f0fe35-9a87-11ea-93fd-0242b9dedf7f", "861b4dae-9a87-11ea-93fd-0242b9dedf7f", "84424766-9a87-11ea-93fd-0242b9dedf7f"]
# scope_id_list = ["866dda8a-9a87-11ea-933d-0a03baec5bce", "8722d21d-9a87-11ea-933d-0a03baec5bce", "e7e0c049-9a87-11ea-933d-0a03baec5bce"]

if not any((proc_id_list, scope_id_list)):
    print("EXIT NOTHING TO PROCESS- Creating Empty report ...")
    # if there is not data, send empty report with all the headers
    edmp_csv_file_name, edmp_dat_file_name = create_edmp_al_report(empty_report=True)
    ftp_transfer(edmp_dat_file_name)
    send_attachment_email([edmp_csv_file_name])

    update_etl_table_sql = f"""UPDATE sc_edmp_al_etl_logs SET status = 'SUCCESS' where id = {MAX_ID};"""
    try:
        pg_conn = psycopg2.connect(f"dbname={pg_cred['dbname']} user={pg_cred['user']} password={pg_cred['password']} host={pg_cred['host']} port={pg_cred['port']} options='-c search_path={pg_cred['schema']}'")
        pg_cur = pg_conn.cursor()
        pg_cur.execute(update_etl_table_sql)
        pg_conn.commit()
        pg_conn.close()
    except Exception as e:
        print(e)
        pg_conn.close()
        sys.exit(1)
    print("DONE")
    sys.exit(1)


proc_id_list_str = get_id_list_as_tuple(proc_id_list)

print("Final proc_inst_id count:", len(set(proc_id_list)))

# this query gives all the case_instance_id for given list of proc_inst_id.
GET_CASE_ID_SQL = f"""
SELECT l.process_instance_id, u.case_instance_id
FROM
    los_application_no l INNER JOIN portal_user u ON l.id::text = u.application_number
WHERE
    l.process_instance_id in {proc_id_list_str};"""

try:
    pg_conn = psycopg2.connect(f"dbname={pg_cred['dbname']} user={pg_cred['user']} password={pg_cred['password']} host={pg_cred['host']} port={pg_cred['port']}")
    pg_cur = pg_conn.cursor()

    pg_cur.execute(GET_CASE_ID_SQL)
    proc_case_list = pg_cur.fetchall()
    pg_conn.close()
except Exception as e:
    print(e)
    print("ERROR IN FETCHING ID LIST")
    error_message = f"ERROR IN FETCHING ID LIST: {e}"
    print(error_message)
    pg_conn.close()
    sys.exit(1)

case_instance_id_list = scope_id_list.copy()
proc_case_zip = dict()  # dictionary for case_instance_id to proc_inst_id. ((Key,Value): (case_instance_id, proc_inst_id))

for i in proc_case_list:
    if i[1]:
        case_instance_id_list.append(i[1])
        proc_case_zip[i[1]] = i[0]  # map case_instace_id with its corresponding proc_inst_id_


scope_id_list = list(set(scope_id_list) | set(case_instance_id_list))
case_instance_id_list = scope_id_list.copy()

scope_id_list_str = get_id_list_as_tuple(scope_id_list)

for case_id in case_instance_id_list:
    if case_id not in proc_case_zip.keys():
        proc_case_zip[case_id] = None

case_instance_id_str = get_id_list_as_tuple(case_instance_id_list)
print("Final scope_id_ count:", len(set(scope_id_list)))
print(f"Final case_instance_id count: {len(case_instance_id_list)}")

# this query gets data from act_hi_varinst for all given proc_inst_id.
GET_VAR_DATA_SQL = f"""SELECT
    PROC_VAR.PROC_INST_ID_ AS PROC_VAR_PROC_INST_ID_ ,
    PROC_VAR.PROCESS_VARIABLE AS PROC_VAR_PROCESS_VARIABLE
FROM
    (
        SELECT
            PROC_INST_ID_,
            (json_object_agg(key, value)) AS PROCESS_VARIABLE
        FROM
            (
                SELECT
                   PROC_INST_ID_,
                   (
                        CASE
                            WHEN VAR_TYPE_ IN ('string')
                                THEN  json_build_object(NAME_, text_)
                            WHEN VAR_TYPE_ IN ('long' , 'integer', 'boolean')
                                 THEN json_build_object(NAME_, LONG_)
                            WHEN VAR_TYPE_ = 'double'
                                 THEN json_build_object(NAME_, DOUBLE_)
                            WHEN VAR_TYPE_ IN ('serializable')
                                 THEN json_build_object(NAME_, BYTEARRAY_ID_)
                        END
                    ) AS json_data
                FROM
                    ACT_HI_VARINST
            ) as A, json_each(json_data)  where PROC_INST_ID_ in {proc_id_list_str}
        GROUP BY
            PROC_INST_ID_
    ) PROC_VAR ;"""

GET_SCOPE_DATA_SQL = f"""SELECT
    SCOPE_VAR.SCOPE_ID_ AS PROC_VAR_PROC_INST_ID_ ,
    SCOPE_VAR.PROCESS_VARIABLE AS PROC_VAR_PROCESS_VARIABLE
FROM
    (
        SELECT
            SCOPE_ID_,
            (json_object_agg(key, value)) AS PROCESS_VARIABLE
        FROM
            (
                SELECT
                   SCOPE_ID_,
                   (
                        CASE
                            WHEN VAR_TYPE_ IN ('string')
                                THEN  json_build_object(NAME_, TEXT_)
                            WHEN VAR_TYPE_ IN ('long' , 'integer', 'boolean')
                                 THEN json_build_object(NAME_, LONG_)
                            WHEN VAR_TYPE_ = 'double'
                                 THEN json_build_object(NAME_, DOUBLE_)
                            WHEN VAR_TYPE_ IN ('serializable')
                                 THEN json_build_object(NAME_, BYTEARRAY_ID_)
                        END
                    ) AS json_data
                FROM
                    ACT_HI_VARINST
            ) as A, json_each(json_data)  where SCOPE_ID_ in {scope_id_list_str}
        GROUP BY
            SCOPE_ID_
    ) SCOPE_VAR ;"""

try:
    pg_conn = psycopg2.connect(f"dbname={pg_cred['dbname']} user={pg_cred['user']} password={pg_cred['password']} host={pg_cred['host']} port={pg_cred['port']}")
    pg_cur = pg_conn.cursor()

    if not proc_id_list_str:
        varinst_data = []
    else:
        pg_cur.execute(GET_VAR_DATA_SQL)
        varinst_data = pg_cur.fetchall()

    if not scope_id_list_str:
        scope_varinst_data = []
    else:
        pg_cur.execute(GET_SCOPE_DATA_SQL)
        scope_varinst_data = pg_cur.fetchall()

    pg_conn.close()
except Exception as e:
    print(e)
    print("ERROR IN FETCHING DATA")
    error_message = f"ERROR IN FETCHING DATA: {e}"
    error_capture(conn, cur, status='FAILED', error_message=error_message)
    pg_conn.close()
    sys.exit(1)


# dump proc varinst data in Dataframe.
def insert_proc_var_data():
    final_proc_var_df = pd.DataFrame(columns=AL_PROC_VARS + ['proc_inst_id'])
    try:
        for i, big_var_data in enumerate(varinst_data):
            proc_inst_id = big_var_data[0]
            var_data = big_var_data[1]
            var_data["proc_inst_id"] = proc_inst_id

            proc_var_df = pd.DataFrame([var_data], columns=AL_PROC_VARS + ['proc_inst_id'])
            final_proc_var_df = final_proc_var_df.append(proc_var_df)
        return final_proc_var_df

    except Exception as e:
        print(e)
        error_message = f"ERROR WHILE INSERTING PROC VAR DATA: {e}"
        print(error_message)
        return False


# dump scope varinst data in Dataframe.
def insert_scope_var_data():
    final_scope_var_df = pd.DataFrame(columns=AL_PROC_VARS + ['scope_id'])
    try:
        for i, big_var_data in enumerate(scope_varinst_data):
            scope_id = big_var_data[0]
            var_data = big_var_data[1]
            var_data["scope_id"] = scope_id

            proc_var_df = pd.DataFrame([var_data], columns=AL_PROC_VARS + ['scope_id'])
            final_scope_var_df = final_scope_var_df.append(proc_var_df)
        return final_scope_var_df

    except Exception as e:
        print(e)
        error_message = f"ERROR WHILE INSERTING PROC VAR DATA: {e}"
        print(error_message)
        return False


try:
    al_proc_df = insert_proc_var_data()
    al_proc_df = al_proc_df[al_proc_df['journeyType'] == 'alDealerJourneyFinal']

    al_scope_df = insert_scope_var_data()
    al_scope_df = al_scope_df[al_scope_df['journeyType'] == 'alDealerJourneyFinal']

    al_scope_df = al_scope_df.astype({'applicationId': str})
    al_proc_df = al_proc_df.astype({'applicationId': str})

    print(f"\nTotal Updated proc_inst_id for alDealerJourneyFinal: {al_proc_df.shape[0]}")
    print(f"Total Updated scope_id for alDealerJourneyFinal: {al_scope_df.shape[0]}\n")
except Exception as e:
    print(e)
    print("ERROR IN CONNECTING TARGET DB")

proc_df_rows = al_proc_df.shape[0]
scope_df_rows = al_scope_df.shape[0]

al_scope_df = al_scope_df.set_index('applicationId')
al_scope_df = al_scope_df[~al_scope_df.index.duplicated(keep="first")]
al_proc_df = al_proc_df.set_index('applicationId')
al_proc_df = al_proc_df[~al_proc_df.index.duplicated(keep="first")]

if scope_df_rows >= proc_df_rows:
    print("scope rows are >= proc rows")
    result_var_df = al_scope_df.reindex(columns=al_scope_df.columns.union(al_proc_df.columns))
    result_var_df.update(al_proc_df, overwrite=False)
    result_var_df.reset_index(inplace=True)
else:
    print("scope rows are < proc rows")
    result_var_df = al_proc_df.reindex(columns=al_proc_df.columns.union(al_scope_df.columns))
    result_var_df.update(al_scope_df, overwrite=True)
    result_var_df.reset_index(inplace=True)

result_var_df['proc_inst_id'].fillna('', inplace=True)
result_var_df['scope_id'].fillna('', inplace=True)

edmp_csv_file_name, edmp_dat_file_name = create_edmp_al_report()

ftp_transfer(edmp_dat_file_name)
print("FTP Transfer to remote server done.")
send_attachment_email([edmp_csv_file_name])
print("Report email sent done.")

update_etl_table_sql = f"""UPDATE sc_edmp_al_etl_logs SET status = 'SUCCESS' where id = {MAX_ID};"""

try:
    pg_conn = psycopg2.connect(f"dbname={pg_cred['dbname']} user={pg_cred['user']} password={pg_cred['password']} host={pg_cred['host']} port={pg_cred['port']}")
    pg_cur = pg_conn.cursor()
    pg_cur.execute(update_etl_table_sql)
    pg_conn.commit()
    pg_conn.close()
except Exception as e:
    print(e)
    pg_conn.close()
    sys.exit(1)