import os
import json
import base64
import http.client
from datetime import datetime

from stanc_db_const import IB_HOST_URL, ENVIRONMENT
from stanc_constants import SENDER_EMAIL, RECIPIENT_EMAIL


def send_attachment_email(attachments):
    """Send email with given attachments.
    :param attachments (list): list of attachment file names. (Strictly need to be list.)
    """
    attachments_name = []
    attachments_content = []
    for attachment in attachments:
        attachment_name = attachment.split('/')[-1]
        attachments_name.append(attachment_name)
        with open(attachment, 'rb') as m:
            attachments_content.append(base64.b64encode(m.read()).decode('utf-8'))

    current_timestamp = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
    integration_slug = "int-mdis-externalcommunication"
    company_slug = "standard-chartered"

    hash = base64.b64encode(f"{integration_slug}-{company_slug}-{current_timestamp}".encode('utf-8')).decode('utf-8')

    current_timestamp_track = datetime.now().strftime("%Y%m%d%H%M%S")
    tracking_id = f"MY{current_timestamp_track}-ACSG-1"
    message_content = base64.b64encode(str("Please Find the reports from " + ENVIRONMENT).encode('utf-8')).decode('utf-8')
    email_subject = base64.b64encode(str("EDMP reports from " + ENVIRONMENT).encode('utf-8')).decode('utf-8')

    conn = http.client.HTTPSConnection(f"{IB_HOST_URL}")
    for recipient_email in RECIPIENT_EMAIL:
        if len(attachments) == 1:
            payload = dict({
                "header": None,
                "body": {
                    "ns1:sendEmail.xmlns:ns1": "http://www.sc.com/GroupFunctions/Communications/v3/ws/provider/ExternalCommunication",
                    "ns1:sendEmail.sendEmailRequest.xmlns:ns1": "http://www.sc.com/SCBML-1",
                    "ns1:sendEmail.sendEmailRequest.xmlns:ns2": "http://www.sc.com/GroupFunctions/Communications/v3/ExternalCommunication",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:captureSystem": "MDIS",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:process.ns1:eventType": "",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns1:payloadFormat": "XML",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns1:payloadVersion": 1,
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:process.ns1:processName": "",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:messageDetails.ns1:messageVersion": 1,
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:trackingId": f"{tracking_id}",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:correlationID": "",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:messageTimestamp": f"{current_timestamp}",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:possibleDuplicate": "false",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:initiatedTimestamp": f"{current_timestamp}",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:messageDetails.ns1:messageType.ns1:typeName": "Communication",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:messageSender.ns1:countryCode": "SG",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:messageSender.ns1:messageSender": "LENDIN",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:messageDetails.ns1:messageType.ns1:subType.ns1:subTypeName": "sendEmail",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:dateTimeStamp": f"{current_timestamp}",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:messageSender.ns1:senderDomain.ns1:domainName": "GroupFunctions",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:uniqueSenderReference": f"{tracking_id}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:Attachments.ns2:messageAttachmentName": f"{attachments_name[0]}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:Attachments.ns2:messageAttachmentContent": f"{attachments_content[0]}",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:messageSender.ns1:senderDomain.ns1:subDomainName.ns1:subDomainType": "Communication",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:language": "ENG",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:mimeType": "N",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:utfIndicator": "Y",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:senderAddress": f"{SENDER_EMAIL}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:messageContent": f"{message_content}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:messageSubject": f"{email_subject}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:toAddress.ns2:toEmailID": f"{recipient_email}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:mulitpleRecepientsIndicator": "Y"
                }
            })

        elif len(attachments) == 2:
            payload = dict({
                "header": None,
                "body": {
                    "ns1:sendEmail.xmlns:ns1": "http://www.sc.com/GroupFunctions/Communications/v3/ws/provider/ExternalCommunication",
                    "ns1:sendEmail.sendEmailRequest.xmlns:ns1": "http://www.sc.com/SCBML-1",
                    "ns1:sendEmail.sendEmailRequest.xmlns:ns2": "http://www.sc.com/GroupFunctions/Communications/v3/ExternalCommunication",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:captureSystem": "MDIS",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:process.ns1:eventType": "",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns1:payloadFormat": "XML",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns1:payloadVersion": 1,
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:process.ns1:processName": "",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:messageDetails.ns1:messageVersion": 1,
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:trackingId": f"{tracking_id}",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:correlationID": "",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:messageTimestamp": f"{current_timestamp}",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:possibleDuplicate": "false",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:initiatedTimestamp": f"{current_timestamp}",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:messageDetails.ns1:messageType.ns1:typeName": "Communication",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:messageSender.ns1:countryCode": "SG",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:messageSender.ns1:messageSender": "LENDIN",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:messageDetails.ns1:messageType.ns1:subType.ns1:subTypeName": "sendEmail",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:dateTimeStamp": f"{current_timestamp}",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:messageSender.ns1:senderDomain.ns1:domainName": "GroupFunctions",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:uniqueSenderReference": f"{tracking_id}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:Attachments[0].ns2:messageAttachmentName": f"{attachments_name[0]}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:Attachments[0].ns2:messageAttachmentContent": f"{attachments_content[0]}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:Attachments[1].ns2:messageAttachmentName": f"{attachments_name[1]}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:Attachments[1].ns2:messageAttachmentContent": f"{attachments_content[1]}",
                    "ns1:sendEmail.sendEmailRequest.ns1:header.ns1:originationDetails.ns1:messageSender.ns1:senderDomain.ns1:subDomainName.ns1:subDomainType": "Communication",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:language": "ENG",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:mimeType": "N",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:utfIndicator": "Y",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:senderAddress": f"{SENDER_EMAIL}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:messageContent": f"{message_content}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:messageSubject": f"{email_subject}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:toAddress.ns2:toEmailID": f"{recipient_email}",
                    "ns1:sendEmail.sendEmailRequest.ns1:sendEmailReqPayload.ns2:sendEmailReq.ns2:communicationInfo.ns2:DestinationChannelInfo.ns2:Email.ns2:mulitpleRecepientsIndicator": "Y"
                }
            })

        headers = {
            'X-Content-Type-Options': 'nosniff',
            'X-Permitted-Cross-Domain-Policies': 'none',
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'sameorigin',
            'Strict-Transport-Security': 'max-age=31536000; includeSubDomains',
            'Content-Security-Policy': 'default-src \'self\'',
            'Content-Type': 'application/json'
        }

        conn.request("POST", f"/ib/api/external-integration/soap/int-mdis-externalcommunication/sendemail/standard-chartered/{hash}/?binding=scbGrpFnCommExtComm_v3_ws_provider_ExternalCommunication_Binder", json.dumps(payload), headers)
        res = conn.getresponse()
        data = res.read()
        print(data.decode("utf-8"))
